
import numpy as np
import cv2
import matplotlib.pyplot as plt
import cPickle
import sys
sys.path.append('/home/jacek/dev/keelab/DeepSport')
from db_metadata import get_projection_matrix, \
                        get_rectangles, \
                        get_detection_head_feet

from deepsport_get_annot import get_gt_s3keys, get_meta_from_s3, get_image_from_s3
from annotations import draw_annotations, extract_annot


def rescale_annotation_prev_format(annot, factor_x, factor_y):
    ax = factor_x
    ay = factor_y

    for k, it in enumerate(annot['annotations']):
        if it['type'] == 'player':
            it['head']['x'] = float(it['head']['x']) * ax
            it['head']['y'] = float(it['head']['y']) * ay
            it['feet']['x'] = float(it['feet']['x']) * ax
            it['feet']['y'] = float(it['feet']['y']) * ay
            it['headOrientation']['x'] = float(it['headOrientation']['x']) * ax
            it['headOrientation']['y'] = float(it['headOrientation']['y']) * ay
            it['hipsOrientation']['x'] = float(it['hipsOrientation']['x']) * ax
            it['hipsOrientation']['y'] = float(it['hipsOrientation']['y']) * ay

        else:
            it['center']['x'] = float(it['center']['x']) * ax
            it['center']['y'] = float(it['center']['y']) * ay


def save_annotated_data_locally(arena, location):
    im_dic = {}
    meta_dic = {}
    annot_dic = {}
    s3keys, num_cam = get_gt_s3keys(arena)
    start_index = 0
    for timestamp in s3keys.keys():
        im_dic[timestamp] = {}
        meta_dic[timestamp] = {}
        annot_dic[timestamp] = {}
        im, metadata, annot = None, None, None
        for viewnum, viewname in [(i, 'camcourt{}'.format(i + 1)) for i in range(0, num_cam)]:
            for el in s3keys[timestamp]:
                if type(el) == str and el.endswith('.json') and el.find(viewname) > 0:  # paths to s3 json
                    metadata = get_meta_from_s3(el)
                elif type(el) == str and el.endswith('_0.png') and el.find(viewname) > 0:  # paths to s3 images
                    im = get_image_from_s3(el)
                elif type(el) == list:  # annotations
                    annot = el[viewnum]

            if not im.size or not metadata or not annot:
                print('error missing metadata or annot or image for ts={}'.format(timestamp))
                continue

            im_dic[timestamp][viewnum] = im
            meta_dic[timestamp][viewnum] = metadata
            annot_dic[timestamp][viewnum] = annot

    output = open('im_dic.pkl', 'wb')
    cPickle.dump(im_dic, output)
    output.close()
    output = open('meta_dic.pkl', 'wb')
    cPickle.dump(meta_dic, output)
    output.close()
    output = open('annot_dic.pkl', 'wb')
    cPickle.dump(annot_dic, output)
    output.close()


def load_annotated_data():
    f1 = open('im_dic.pkl','rb')
    f2 = open('meta_dic.pkl', 'rb')
    f3 = open('annot_dic.pkl', 'rb')
    im_dic = cPickle.load(f1)
    meta_dic = cPickle.load(f2)
    annot_dic = cPickle.load(f3)
    f1.close()
    f2.close()
    f3.close()

    for ts in im_dic.keys():
        for view in im_dic[ts]:
            im = im_dic[ts][view]
            annot = annot_dic[ts][view]
            metadata = meta_dic[ts][view]
            rescale_annotation_prev_format(annot, im.shape[1], im.shape[0])
            draw_annotations(im, annot)
            cv2.imshow('s3', im)
            cv2.imwrite('images/im_{}_{}.png'.format(view,ts), im)
            pressed = cv2.waitKey()


def plot_pitch_hockey(c='b'):
    ll = '--'
    plt.plot([426, 5669], [0, 0], c+ll)
    plt.plot([426, 5669], [2590, 2590], c+ll)
    plt.plot([3033, 3033], [0, 2590], c+ll)
    plt.plot([3063, 3063], [0, 2590], c+ll)


def plot_all_players():
    f2 = open('meta_dic.pkl', 'rb')
    f3 = open('annot_dic.pkl', 'rb')
    meta_dic = cPickle.load(f2)
    annot_dic = cPickle.load(f3)
    f2.close()
    f3.close()

    for ts in sorted(meta_dic.keys()):
        for view in meta_dic[ts]:

            annot = annot_dic[ts][view]
            metadata = meta_dic[ts][view]
            rescale_annotation_prev_format(annot, 2054, 2456)
            det_head_feet = get_detection_head_feet(metadata, min_score=0.140, world_coord=True)
            P = get_projection_matrix(metadata)

            head_feet, teams, head_angles, ball = extract_annot(annot, P)

            plot_pitch_hockey()
            plot_players(head_feet, teams)

            cols = ['k','b','r']
            for p1,p2,team in zip(head_feet, head_angles, teams):
                #compute offset between head and head_orient
                off =  (p2[0] - p1[0], p2[1] - p1[1])
                #add this offset to feet coord
                feetoff = (off[0] + p1[2], off[1] + p1[3])
                plt.plot((p1[2],feetoff[0]),(p1[3],feetoff[1]), cols[team])
            if len(ball):
                plt.plot(ball[0],ball[1],'*y')

        plt.show()


def plot_players(head_feet, teams):
    xgt = [df[2] for df in head_feet]
    ygt = [df[3] for df in head_feet]

    other = ([], [])
    team1 = ([], [])
    team2 = ([], [])
    for x, y, t in zip(xgt, ygt, teams):
        if t == 0:
            other[0].append(x)
            other[1].append(y)
        elif t == 1:
            team1[0].append(x)
            team1[1].append(y)
        else:
            team2[0].append(x)
            team2[1].append(y)
    plt.plot(team1[0], team1[1], 'bo')
    plt.plot(team2[0], team2[1], 'ro')
    plt.plot(other[0], other[1], 'ko')


def find_coa(coords):
    coords.sort()
    z = []
    xmidd = []
    thr_cm = 10.0 # orientation smaller than this number are not counted for coa
    for p, q in zip(coords[:-1], coords[1:]):
        middle_pt = (p[0] + q[0]) / 2.0
        zone = [0, 0]
        # print (middle_pt)
        # print ([x[0] for x in players_x])
        # print ([x[1] for x in players_x])
        for r in coords:
            # r[1] is orientation
            if r[1] > thr_cm and r[0] < middle_pt:
                zone[0] += 1
            elif r[1] < -thr_cm and r[0] > middle_pt:
                zone[1] += 1
        print('zone ({},{})'.format(zone[0], zone[1]))
        z.append(np.sum(zone))
        xmidd.append(middle_pt)

    ii = np.argmax(z)
    mm = z[ii]
    z[ii] = 0
    ii2 = np.argmax(z)
    a = False
    if z[ii2] == mm:
        print('2 max found')
        a = True
    return [xmidd[ii], xmidd[ii2], a]

def centre_of_att():
    f2 = open('meta_dic.pkl', 'rb')
    f3 = open('annot_dic.pkl', 'rb')
    meta_dic = cPickle.load(f2)
    annot_dic = cPickle.load(f3)
    f2.close()
    f3.close()

    i = 0
    for ts in sorted(meta_dic.keys()):
        players_x = []
        players_y = []
        ball = []
        imlist = []

        for view in meta_dic[ts]:
            annot = annot_dic[ts][view]
            metadata = meta_dic[ts][view]
            rescale_annotation_prev_format(annot, 2054, 2456)
            P = get_projection_matrix(metadata)
            head_feet, teams, head_angles, ball_0 = extract_annot(annot, P, use_head=False)
            if len(ball_0):
                ball = ball_0

            if 1:
                im = cv2.imread('images/im_{}_{}.png'.format(view,ts))
                draw_annotations(im, annot)
                imlist.append( cv2.resize(im, None, None, 0.25, 0.25, cv2.INTER_AREA))
                #cv2.imshow('cc', im)
                #cv2.waitKey()

            for h,t,h2 in zip(head_feet,teams, head_angles):
                players_x.append((h[2], h2[0] - h[0], t))
                players_y.append((h[3], h2[1] - h[1], t))
            plot_players(head_feet, teams)

        cols = ['k', 'b', 'r']
        for p,q in zip(players_x,players_y):
            assert(p[2] == q[2])
            plt.plot((p[0],p[0]+p[1]), (q[0],q[0]+q[1]), cols[p[2]])

        xmidd = find_coa(players_x)
        ymidd = find_coa(players_y)

        linetypex = '-' if xmidd[2] else ':'
        linetypey = '-' if ymidd[2] else ':'

        plt.plot([xmidd[0],xmidd[0]], [0, 2500], 'b')
        plt.plot([xmidd[1], xmidd[1]], [0, 2500] ,'b' + linetypex)
        plt.plot([0, 5000], [ymidd[0], ymidd[0]], 'b')
        plt.plot([0, 5000], [ymidd[1], ymidd[1]], 'b' + linetypey)

        plot_pitch_hockey('g')

        if len(ball):
            plt.plot(ball[0], ball[1], '*y')

        plt.title('im {}'.format(ts))
        plt.gca().invert_yaxis()

        #save figure
        plt.savefig('plot_hc_{:03}'.format(i))
        cv2.imwrite('im_hc_{:03}.png'.format(i), np.hstack((imlist[0], imlist[1], imlist[2])))

        i += 1
        plt.show()

if __name__ == '__main__':
    arena = 'KS-US-HARBORCENTER1'

    #save_annotated_data_locally(arena, None)
    #load_annotated_data()
    centre_of_att()
    #plot_all_players()

