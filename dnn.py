import numpy as np
import argparse
import cv2
import time

# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))


def apply_dnn(net, image):
    start = time.time()
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 0.007843, (300, 300), 127.5)

    net.setInput(blob)
    detections = net.forward()

    end = time.time()
    print("elapsed {}".format(1000 * (end - start)))

    (h, w) = image.shape[:2]

    # loop over the detections
    for i in np.arange(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence > 0.2:
            # extract the index of the class label from the `detections`,
            # then compute the (x, y)-coordinates of the bounding box for
            # the object
            idx = int(detections[0, 0, i, 1])
            if idx == 1 or idx == 11:
                continue
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # display the prediction
            label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
            print("[INFO] {}".format(label))
            cv2.rectangle(image, (startX, startY), (endX, endY),
                          COLORS[idx], 2)
            y = startY - 15 if startY - 15 > 15 else startY + 15
            cv2.putText(image, label, (startX, y),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)




cap = cv2.VideoCapture('/home/jacek/mygraphs/img/data/clipped.mp4')

print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe('/home/jacek/libs/MobileNet-SSD/MobileNetSSD_deploy.prototxt', '/home/jacek/Downloads/MobileNetSSD_deploy.caffemodel')


while(cap.isOpened()):
    ret, frame = cap.read()

    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    small_im =  frame[200:600,200:600]

    apply_dnn(net, small_im)

    cv2.imshow('frame',small_im)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
exit(0)



imagefull = cv2.imread('/tmp/spiroudome_20111217/ROIFrames/View1/Images_Images/arena123_game11111_VB_1324150789812_v1.png')


image = imagefull[400:700,700:1000]

(h, w) = image.shape[:2]



# show the output image
cv2.imshow("Output", image)
cv2.waitKey(0)

