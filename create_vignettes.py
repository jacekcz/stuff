
import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
sys.path.append('/home/jacek/dev/keelab/DeepSport')

import numpy
import cv2
import json
import os, glob

from hog import HOGDetect
from db_metadata import get_projection_matrix

roifilerroot = '/tmp/spiroudome_20111217/ROIFrames/'
image_dir_name = 'Images_Images'



def project(P,X):
    x = numpy.dot(P,X)
    x /= x[2]
    return x[0:2]

def find_metafiles(root_dir,image_dir):
    """
    Find all metadata files (json) saved by roifiler
    in directory root_dir/View*/image_dir/*.json
    """
    metafiles = glob.glob(root_dir + '/View?/' + image_dir + '/*.json')
    return metafiles


def get_vignettes_from_file(metafile, min_score=0.1, offset=20.):
    with open(metafile) as f:
        meta = json.load(f)
        return get_vignettes(meta, min_score, offset)


def expected_head_point(P, feet):
    Y = numpy.array([feet[0],feet[1]] + [-160, 1.])
    return project(P,Y)

"""
Return detection vignettes
@param : metadata : metadata
@param : min_score : minimum detection score
@param : offset  : offset added around detection (cm in world coord)
@return : rects : list of rectangle around the detection
        : G     : full image
"""
def get_vignettes(metadata, min_score=0.1, offset=20.):
    ts_det = metadata['timestamp_detections']
    view = metadata['view']
    prefix = metadata['prefix']
    ts_0 = metadata['timestamp_0']
    filename = prefix + '{}'.format(ts_0) + '_v{}'.format(view) + '.png'
    im_path = os.path.join(roifilerroot, 'View{}'.format(view), image_dir_name, filename)

    get_k = lambda KK,n: numpy.array(metadata['calibration'][KK]).reshape((3,n))
    K = get_k('KK',3)
    R = get_k('R', 3)
    t = get_k('T', 1)
    P = numpy.hstack( [R,t] )
    P = numpy.dot(K,P)
    print (im_path)

    players = metadata['players']
    feet, levels = [],[]
    for pl in players:
        if pl['level'] >= min_score:
            feet.append( pl['pos_feet'])
            levels.append(pl['level'])

    im_feet, im_head, im_width = [], [], []
    for ff in feet:
        X = numpy.array(ff + [0. + offset,1.])
        Y = numpy.array(ff + [-180 - offset, 1.])

        x,y = project(P,X),project(P,Y)
        im_feet.append(x)
        im_head.append(y)
        X[0] -= 40 - offset
        x2 = project(P, X)
        X[0] += 80 + 2*offset
        x3 = project(P, X)
        width = int(round(x3[0] - x2[0]))
        im_width.append(width)

    G = cv2.imread(im_path)

    rects = []
    draw = False
    #print (len(players), len(feet), len(im_feet), len(im_head), len(levels), len(im_width))
    for x,y,lv,w in zip(im_feet, im_head, levels, im_width):
        to_point = lambda x:(int(round(x[0])), int(round(x[1])))
        p1 = to_point(x)
        p2 = to_point(y)
        if draw:
            cv2.circle(G,p1, 3 + int(lv*10), (255,255,0))
            cv2.line(G, p1, p2, (255, int(lv*50),0), 1)
            cv2.rectangle(G, (p2[0]-w/2, p2[1]), (p1[0]+w/2, p1[1]), (0,255,0))
        rects.append((p2[0]-w/2, p2[1], p1[0]+w/2, p1[1]))
    cv2.imshow('spirou',G)
    cv2.waitKey(0)

    return rects, G


if __name__ == '__main__':

    files = find_metafiles(roifilerroot, image_dir_name)

    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    for fl in files:
        rects, im = get_vignettes_from_file(fl)

        for r in rects:
            img = im[r[1]:r[3], r[0]:r[2]]
            if img.size and r[0] >= 0 and r[2] >= 0:
                HOGDetect(img, hog)
