import cv2
import numpy as np
import sys
import math
#import matplotlib.pyplot as plt
sys.path.append('/home/jacek/dev/keelab/DeepSport')
from db_metadata import get_projection_matrix, \
                        get_detection_head_feet, \
                        get_rectangles

from create_vignettes import expected_head_point
from deepsport_get_annot import get_gt_s3keys, \
                                get_s3resource, \
                                s3rootkey, \
                                get_meta_from_s3, \
                                get_image_from_s3, \
                                get_arena_list
from label_dist import LabelDist
from annotations import rescale_annotation, \
                        draw_annotations, \
                        extract_annot, \
                        extract_head_feet


def l2_dist(p1,p2):
    return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)


def greedy_matching(Dist, thr=0.01):
    matches = []

    if Dist.data:
        amin_dist = np.argmin(Dist, axis=1)
        min_dist = np.min(Dist, axis=1)

        for i,val in enumerate(min_dist):
            if val < thr:
                matches.append((i, amin_dist[i]))
    else:
        matches = []

    return matches


def save_sub_image(im, rect, path):
    r = (max(0,rect[0]),
         max(0,rect[1]),
         min(im.shape[1],rect[2]),
         min(im.shape[0],rect[3]))

    img = im[r[1]:r[3], r[0]:r[2]]

    if img.size:
        show = False
        if show:
            cv2.imshow('im', img)
            cv2.waitKey(2)
        cv2.imwrite(path, img)


def save_positives(im, head_feet, margin_in, start_index=0):
    i = start_index
    for hf in head_feet:

        d = l2_dist(hf[:2], hf[2:])
        margin = int(margin_in * d)
        margin_y = int(margin_in * d * 0.8)  # use a slightly smaller margin vertically

        min_x = int(min((hf[0],hf[2])))
        min_y = int(min((hf[1],hf[3])))

        max_x = int(max((hf[0], hf[2])))
        max_y = int(max((hf[1], hf[3])))

        r = (max((0,min_x - margin)),
             max((0,min_y - margin_y)),
             min((im.shape[1],max_x + margin)),
             min((im.shape[0],max_y + margin_y)))

        img = im[r[1]:r[3], r[0]:r[2]]

        #if img.size and r[0] >= 0 and r[2] >= 0:
        cv2.imshow('im', img)
        cv2.imwrite('/home/jacek/data/training/pos/im_{0:05d}.png'.format(i), img)
        i += 1
        cv2.waitKey(2)
    return i


def save_negatives(im, rects, start_index):
    i = start_index
    for r in rects:

        img = im[r[1]:r[3], r[0]:r[2]]
        if img.size and r[0] >= 0 and r[2] >= 0:
            cv2.imwrite('/home/jacek/data/training/neg/im_{0:05d}.png'.format(i), img)
            i += 1

    return i


def compute_matching(det, gt, im_size, thr):
    ld = LabelDist(dist='l2w')
    N = len(det)
    M = len(gt)
    Dist = np.zeros((N,M))
    for i,x in enumerate(det):
        xn = [x[0]/im_size[1], x[1]/im_size[0], x[2]/im_size[1], x[3]/im_size[0]]
        for j,y in enumerate(gt):
            yn = [y[0]/im_size[1], y[1]/im_size[0], y[2]/im_size[1], y[3]/im_size[0]]
            Dist[i,j] = ld.compute(xn,yn,(2,0.5))
    #print(Dist)
    return greedy_matching(Dist,thr)


def do_matching_in_world(det, gt, thr_cm):
    N = len(det)
    M = len(gt)
    Dist = np.zeros((N,M))
    for i,x in enumerate(det):
        for j,y in enumerate(gt):
            Dist[i,j] = l2_dist(x[2:4], y[2:4])
    #print(Dist)
    return greedy_matching(Dist,thr_cm)


def show_match_based_training_set(arena):
    """
    Show a training set by matching detections and ground truth
    """
    s3keys, num_cam = get_gt_s3keys(arena)
    start_index = 0
    start_index_neg = 0

    for timestamp in s3keys.keys():
        im, metadata, annot = None,None,None
        for viewnum, viewname in [(i, 'camcourt{}'.format(i + 1)) for i in range(0, num_cam)]:
            for el in s3keys[timestamp]:
                if type(el) == str and el.endswith('.json') and el.find(viewname)>0:  # paths to s3 json
                    metadata = get_meta_from_s3(el)
                elif  type(el) == str and el.endswith('_0.png')and el.find(viewname)>0:  # paths to s3 images
                    im = get_image_from_s3(el)
                elif type(el) == list:  # annotations
                    annot = el[viewnum]

            if not im.size or not metadata or not annot:
                print('error missing metadata or annot or image for ts={}'.format(timestamp))
                continue

            # annotation are normalised
            rescale_annotation(annot, im.shape[1], im.shape[0])
            min_score = 0.14
            rects = get_rectangles(metadata, min_score=min_score, offset=0)
            # 3d detections, must be the same across all views (to do check this)
            det_head_feet = get_detection_head_feet(metadata, min_score=min_score, world_coord=True)
            det_head_feet_im = get_detection_head_feet(metadata, min_score=min_score)
            #head_feet = extract_head_feet(annot)
            P = get_projection_matrix(metadata)
            head_feet = extract_head_feet(annot, projection=P, world_coord=True)
            head_feet_im = extract_head_feet(annot)
            matches = do_matching_in_world(det_head_feet, head_feet, 75.)

            if 0: # enable to save image
                start_index = save_positives(im, head_feet, 0.25, start_index)
                start_index_neg = save_negatives(im, rects, start_index_neg)

            xdet = [df[2] for df in det_head_feet]
            ydet = [df[3] for df in det_head_feet]
            xgt = [df[2] for df in head_feet]
            ygt = [df[3] for df in head_feet]

            # plot detection and gt on ground plane
            plot = False
            if plot:
                plt.plot(xdet,ydet,'r.')
                plt.plot(xdet, ydet, 'r.')
                plt.plot(xgt, ygt, 'b.')
                plt.plot(xgt, ygt, 'b.')

                for i, match in enumerate(matches):
                    dhf = det_head_feet[match[0]]
                    hf = head_feet[match[1]]
                    plt.plot(dhf[2], dhf[3], '*')
                    plt.plot(hf[2], hf[3], 'c')
                plt.show()

            for r in rects:
                cv2.rectangle(im, r[0:2], r[2:], (0, 255, 0))

            colors = [(0, 0, 255), (127, 0, 127), (127, 127, 0), (0, 127, 127)]
            for i,match in enumerate(matches):
                dhf = det_head_feet_im[match[0]]
                dhf = map(int,dhf)
                hf = head_feet_im[match[1]]
                hf = map(int,hf)
                cv2.line(im, tuple(dhf[0:2]), tuple(dhf[2:]), colors[i%len(colors)], 2)
                cv2.line(im, tuple(hf[0:2]), tuple(hf[2:]), colors[i%len(colors)], 2)

            draw_annotations(im, annot)
            cv2.imshow('s3', im)
            pressed = cv2.waitKey()
            print(pressed)

def make_training_set_gt(sport):
    """
    Extract Positive vignettes around players using ground truth
    """
    horiz_prop = 0.30 # horiz proportion of player height to create the bounding box (0.25 for BB)
    vertical_prop = 0.20 # vert proportion of player height to create the bounding box (0.15 for BB)
    arenas = get_arena_list()
    index = 0
    basepath = '/home/jacek/data/training/'
    path = basepath + 'basket/' if sport == 'BB' else basepath + 'hockey/'
    with open(basepath + 'train.csv', 'w') as f:
        f.write('@FILE\tSTRING\n')
        for arena in arenas:
            if sport == 'BB':
                # skip ice hockey for now (harbour centre and capitol)
                if arena == 'KS-US-HARBORCENTER1' or \
                            arena == 'KS-US-HARBORCENTER2' or \
                            arena == 'KS-US-CAPITALS':
                    print('Skipping {}'.format(arena))
                    continue
            elif sport == 'IH':
                # keep ice hockey for now (harbour centre and capitol)
                if not (arena == 'KS-US-HARBORCENTER1' or
                        arena == 'KS-US-HARBORCENTER2' or
                        arena == 'KS-US-CAPITALS'):
                    print('Skipping {}'.format(arena))
                    continue
            else:
                raise(RuntimeError('Unknow sport {}'.format(sport)))

            if sport == 'BB':
                s3keys, num_cam = get_gt_s3keys(arena, max_annotation_ts=1513694341000) # before 20/12/2017
            elif sport == "IH":
                s3keys, num_cam = get_gt_s3keys(arena, max_annotation_ts=1513393341000)  # see save_hockey_test_images()
            else:
                raise (RuntimeError('Unknow sport {}'.format(sport)))
            for timestamp in s3keys.keys():
                im, metadata, annot = None,None,None
                for viewnum, viewname in [(i, 'camcourt{}'.format(i + 1)) for i in range(0, num_cam)]:
                    for el in s3keys[timestamp]:
                        if type(el) == str and el.endswith('.json') and el.find(viewname)>0:  # paths to s3 json
                            metadata = get_meta_from_s3(el)
                        elif  type(el) == str and el.endswith('_0.png')and el.find(viewname)>0:  # paths to s3 images
                            im = get_image_from_s3(el)
                        elif type(el) == list:  # annotations
                            annot = el[viewnum]

                    if not im.size or not metadata or not annot:
                        print('error missing metadata or annot or image for ts={}'.format(timestamp))
                        continue

                    # annotation are normalised
                    rescale_annotation(annot, im.shape[1], im.shape[0])

                    P = get_projection_matrix(metadata)
                    head_feet = extract_head_feet(annot, projection=P, world_coord=True)
                    head_feet_im = extract_head_feet(annot)

                    _, teams, _, _ = extract_annot(annot, P)

                    # team 0,1 => players and team 2 => referees or others
                    player_types = [2 if x == 0  else 1 for x in teams]

                    xgt = [df[2] for df in head_feet]
                    ygt = [df[3] for df in head_feet]

                    for df, ptype in zip(head_feet_im, player_types):
                        h = abs(df[3] - df[1])
                        to_point = lambda x:(int(round(x[0])), int(round(x[1])))
                        left = min(df[0], df[2])
                        right = max(df[0], df[2])
                        upper_left = to_point ([left - h * horiz_prop, df[1] - h * vertical_prop])
                        lower_right = to_point ([right + h * horiz_prop, df[3] + h * vertical_prop])
                        #cv2.rectangle(im, upper_left, lower_right, (0, 255, 0))
                        name = path + 'im_{0:06d}_{1}.png'.format(index, ptype)
                        save_sub_image(im, upper_left + lower_right, name)
                        ss = 'basket/image_{0:06d}_{1}.png {2}\n'.format(index, ptype, ptype)
                        f.write(ss)
                        index += 1

                    #draw_annotations(im, annot)
                    #cv2.imshow('s3', im)
                    #pressed = cv2.waitKey()
                    #print(pressed)
    f.close()
    return index


def subimage(image, centre, theta, width, height):
    """ Extract a rotated rectangle (subimage) from image
        image : image from which subim is extracted
        centre : centre of subim
        theta : how subim is rotated
        width,height : size of subim
    """
    v_x = (np.cos(theta), np.sin(theta))
    v_y = (-np.sin(theta), np.cos(theta))
    s_x = centre[0] - v_x[0] * ((width-1) / 2) - v_y[0] * ((height-1) / 2)
    s_y = centre[1] - v_x[1] * ((width-1) / 2) - v_y[1] * ((height-1) / 2)

    mapping = np.array([[v_x[0],v_y[0], s_x],
                        [v_x[1],v_y[1], s_y]])

    return cv2.warpAffine(image,
                          mapping,
                          (width, height),
                          flags=cv2.WARP_INVERSE_MAP+cv2.INTER_AREA,
                          borderMode=cv2.BORDER_REPLICATE)


def extract_vignette(image, P, feet_image, feet_world):
    # find point at 180cm above feet point
    head = expected_head_point(P,feet_world)

    theta = np.arctan2(head[1] - feet_image[1],head[0] - feet_image[0])
    centre = (head + feet_image)/2.0
    height = int(round(l2_dist(head,feet_image)*1.3))
    width = int(round(height/2.0))
    vignette = subimage(image, centre, theta + np.pi/2.0, width, height)
    return vignette


def show_positive_vignettes(arena):
    """
    Extract and show a ROTATED vignette around each player
    """
    s3keys, num_cam = get_gt_s3keys(arena)
    for timestamp in s3keys.keys():
        im, metadata, annot = None, None, None
        for viewnum, viewname in [(i, 'camcourt{}'.format(i+1)) for i in range(0,num_cam)]:
            for el in s3keys[timestamp]:
                if type(el) == str and el.endswith('.json') and el.find(viewname) > 0:  # paths to s3 json
                    metadata = get_meta_from_s3(el)
                elif type(el) == str and el.endswith('_0.png') and el.find(viewname) > 0:  # paths to s3 images
                    im = get_image_from_s3(el)
                elif type(el) == list:  # annotations
                    annot = el[viewnum]

            if not im.size or not metadata or not annot:
                print('error missing metadata or annot or image for ts={}'.format(timestamp))
                continue

            # annotation are normalised
            rescale_annotation(annot, im.shape[1], im.shape[0])
            P = get_projection_matrix(metadata)
            head_feet = extract_head_feet(annot, projection=P, world_coord=True)
            head_feet_im = extract_head_feet(annot)

            for hf_im,hf_wd in zip(head_feet_im, head_feet):
                vig = extract_vignette(im, P, np.array(hf_im[2:4]), np.array(hf_wd[2:4]))
                cv2.imshow('vig', vig)
                pressed = cv2.waitKey()

            draw_annotations(im, annot)
            cv2.imshow('s3', im)
            pressed = cv2.waitKey()
            print(pressed)


def ppp():
    with open('test.txt','w') as f:
        f.write('@FILE\tSTRING\n')
        for k in range(0,100):
            ss = 'trainpos/image_{:05d}.png  1  \n'.format(k)
            f.write(ss)

    f.close()


if __name__ == '__main__':
    #arena = 'KS-US-HARBORCENTER1'
    #arena = 'KS-FR-BOURGEB'
    #training_set_positive(arena)
    #make_training_set(arena)

    if 1:
        sp = 'IH'  # 'BB' or 'IH'
        num=make_training_set_gt('IH')
        print('Found {} images'.format(num))
