
import sys
sys.path.append('/home/jacek/dev/director/deps/ffmpeg-2.8.3/lib/')
print sys.path
import cv2
import numpy as np
from time import clock
import os

""" 
Lukas-Kanade sparse optical flow on image pairs in a folder
"""


### parameters ###
lk_params = dict( winSize  = (15, 15),
                  maxLevel = 2,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

feature_params = dict( maxCorners = 5000,
                       qualityLevel = 0.0003,
                       minDistance = 7,
                       blockSize = 7 )

def compute_optical_flow(frame1,frame2,downscale=True):
    """
    Compute LK optical flow
    :param frame1: image 1
    :param frame2: image 2
    :return: images with lines between features
    """
    tracks = []
    frame_gray = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)

    p = cv2.goodFeaturesToTrack(frame_gray, **feature_params)
    if p is not None:
        for x, y in np.float32(p).reshape(-1, 2):
            tracks.append([(x, y)])
            # for pt in p:
            #    cv2.circle(frame1, tuple(pt[0]), 3, (255,0,0))

    img1 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
    img0 = frame_gray
    p0 = np.float32([tr[-1] for tr in tracks]).reshape(-1, 1, 2)
    p1, st, err = cv2.calcOpticalFlowPyrLK(img0, img1, p0, None, **lk_params)
    p0r, st, err = cv2.calcOpticalFlowPyrLK(img1, img0, p1, None, **lk_params)
    d = abs(p0 - p0r).reshape(-1, 2).max(-1)
    good = d < 2

    print("Found {} features".format(len(good)))
    factor = 2. # extends the length of the motion for vizualisation

    if downscale:
        color_frame1 = cv2.resize(frame1, None, None, 0.5, 0.5)
        color_frame2 = cv2.resize(frame2, None, None, 0.5, 0.5)
        for pt0, pt1, good_flag in zip(p0, p1, good):
            if good_flag:
                # d = pt1 - pt0
                # pt1 = pt0 + *d
                cv2.line(color_frame1, tuple(np.int32(pt0 * .5)[0]), tuple(np.int32(pt1 * .5)[0]), (0, 255, 0), 1)

        for pt0, pt1, good_flag in zip(p0, p1, good):
            if good_flag:
                d = pt1 - pt0
                pt1 = pt0 + factor * d
                cv2.line(color_frame2, tuple(np.int32(pt0 * .5)[0]), tuple(np.int32(pt1 * .5)[0]), (0, 255, 0), 1)
                # else:
                # cv2.line(frame1, tuple(np.int32(pt0)[0]), tuple(np.int32(pt1)[0]), (0, 0, 255), 1)
    else:
        color_frame1 = frame1
        color_frame2 = frame2
        for pt0, pt1, good_flag in zip(p0, p1, good):
            if good_flag:
                d = pt1 - pt0
                pt1 = pt0 + factor * d
                cv2.line(color_frame1, tuple(np.int32(pt0)[0]), tuple(np.int32(pt1)[0]), (0, 255, 0), 1)
        for pt0, pt1, good_flag in zip(p0, p1, good):
            if good_flag:
                d = pt1 - pt0
                pt1 = pt0 + factor * d
                cv2.line(color_frame2, tuple(np.int32(pt0)[0]), tuple(np.int32(pt1)[0]), (0, 255, 0), 1)
                # else:
                # cv2.line(frame1, tuple(np.int32(pt0)[0]), tuple(np.int32(pt1)[0]), (0, 0, 255), 1)
    return color_frame1, color_frame2


def do_spirou():
    frame1 = cv2.imread('/home/jacek/libs/pyflow/arenaARENAID_game11111_BB_1324150748544_v2.png')
    frame2 = cv2.imread('/home/jacek/libs/pyflow/arenaARENAID_game11111_BB_1324150748594_v2.png')
    x1,x2=400,780
    y1,y2=600,1200
    color_frame1, color_frame2 = compute_optical_flow(frame1[x1:x2,y1:y2], frame2[x1:x2,y1:y2], downscale=False)
    cv2.imshow('sdf', color_frame2)
    cv2.imwrite('lk_opt_flow.png', color_frame2)
    pressed = cv2.waitKey()


if __name__ == '__main__':

    directory = '/home/jacek/data/harborcenter1/view1/'
    files = sorted(os.listdir(directory))
    for fil1,fil2 in zip(files[::2],files[1::2]):
        if fil1.endswith('.png') and fil2.endswith('.png'):
            frame1 = cv2.imread(directory + fil1)
            frame2 = cv2.imread(directory + fil2)
            color_frame1, color_frame2 = compute_optical_flow(frame1,frame2)

            while 1:
                cv2.imshow('sdf', color_frame1)
                print('press <space> to move to next image pair')
                print('press any other key to show the other image')
                pressed = cv2.waitKey()
                print(pressed)
                if pressed == 32:
                    break
                cv2.imshow('sdf', color_frame2)
                pressed = cv2.waitKey()

