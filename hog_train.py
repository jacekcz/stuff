import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')

import numpy
import cv2
import glob, random

def make_hog_full(hog, dir, flip=False):
    image_list = glob.glob(dir)

    features = []
    for imf in image_list:
        img = cv2.imread(imf)

        if img.shape != (128,64,3):
            tmp = cv2.resize(img, (64, 128), 0., 0., cv2.INTER_AREA)
            img = tmp

        assert (img.shape == (128, 64, 3))
        feat = hog.compute(img, (8, 8), (0, 0))
        features.append(feat[:,0])
        if flip:
            img2 = cv2.flip(img, 0)
            feat = hog.compute(img2, (8, 8), (0, 0))
            features.append(feat[:, 0])

    return features

def make_hog_sub(hog, dir, num=10):
    image_list = glob.glob(dir)
    features = []

    for imf in image_list:
        img = cv2.imread(imf)

        (h,w,r) = img.shape
        for k in range(num):
            col = random.randrange(64,w)
            row = random.randrange(128,h)

            subim = img[row-128:row,col-64:col]

            if 0:
                cv2.imshow('xcv', subim)
                cv2.waitKey(0)

            feat = hog.compute(subim, (8, 8), (0, 0))
            features.append(feat[:, 0])

    return features

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.linear_model import SGDClassifier

from sklearn import preprocessing
from sklearn import metrics

import matplotlib.pyplot as plt
import os

"""
Load local dataset 
"""
def load_data(set_type):
    hog = cv2.HOGDescriptor()

    precomputed = False

    if set_type == 'inria_train':
        if os.path.exists('./inria_train.npz'):
            dic = numpy.load('./inria_train.npz')
            X = dic['X']
            y = dic['y']
            precomputed = True
        else:
            features_pos = make_hog_full(hog, './crop/*.png', flip=True)
            features_neg = make_hog_sub(hog, '/home/jacek/data/INRIAPerson/Train/neg/*')
    elif set_type == 'inria_test':
        if os.path.exists('./inria_test.npz'):
            dic = numpy.load('./inria_test.npz')
            X = dic['X']
            y = dic['y']
            precomputed = True
        else:
            features_pos = make_hog_full(hog, './croptest/*.png')
            features_neg = make_hog_sub(hog, '/home/jacek/data/INRIAPerson/Test/neg/*')

    elif set_type == 'basket':
        if os.path.exists('./basket.npz'):
            dic = numpy.load('./basket.npz')
            X = dic['X']
            y = dic['y']
            precomputed = True
        else:
            features_pos = make_hog_full(hog, '/home/jacek/data/training/pos/*.png',flip=True)
            features_neg = make_hog_full(hog, '/home/jacek/data/training/neg/*.png', flip=True)
    else:
        print('unknown dataset')
        return [],[]

    if not precomputed:
        print(len(features_neg))
        print(len(features_pos))

        X = features_pos + features_neg
        y = len(features_pos) * [1] + len(features_neg) * [0]

        numpy.savez('./' + set_type + '.npz', X=X, y=y)

    return X, y


"""
Compare classifiers
"""
def train_all_classifiers():
    X_train, y_train = load_data('inria_train')
    X_test, y_test = load_data('inria_test')

    rescale = 0
    if rescale:
        scaler = preprocessing.StandardScaler().fit(X_train)
        scaler.transform(X_train)
        scaler.transform(X_test)

    names = [  # "Linear SVM", "RBF SVM", #"Gaussian Process",
        "SGD",
        "Decision Tree", "Random Forest", "Neural Net", "AdaBoost",
        "Naive Bayes", "QDA"]

    classifiers = [
        SGDClassifier(loss="hinge", penalty="l2"),
        # SVC(kernel="linear", C=0.025),
        # SVC(gamma=2, C=1),
        # GaussianProcessClassifier(1.0 * RBF(1.0)),
        DecisionTreeClassifier(max_depth=5),
        RandomForestClassifier(n_estimators=15),
        MLPClassifier(alpha=1),
        AdaBoostClassifier(),
        GaussianNB(),
        QuadraticDiscriminantAnalysis()]

    # iterate over classifiers
    for name, clf in zip(names, classifiers):
        clf.fit(X_train, y_train)
        score = clf.score(X_test, y_test)
        y_pred = clf.predict(X_test)

        print (name, score)
        print(metrics.classification_report(y_test, y_pred, target_names=['pos', 'neg']))
        print(metrics.confusion_matrix(y_test, y_pred, labels=range(2)))


def train_sgd(X_train, y_train):
    clf = SGDClassifier(loss="hinge", penalty="l2")
    clf.fit(X_train, y_train)
    return clf


def evaluate():
    X_train, y_train = load_data('inria_train')
    sgd = train_sgd(X_train, y_train)
    X_test, y_test = load_data('basket')
    score = sgd.score(X_test, y_test)
    y_pred = sgd.predict(X_test)

    y_score = sgd.decision_function(X_test)

    score_pos = y_score[numpy.array(y_test) == 1]
    score_neg = y_score[numpy.array(y_test) == 0]

    print (score_pos)
    all = numpy.hstack((score_pos,score_neg))
    min_score = numpy.min(all)
    max_score = numpy.max(all)
    bins = numpy.arange(min_score,max_score, (max_score-min_score)/100.)

    x2 = plt.hist(score_neg,bins=bins,color='blue')
    x1 = plt.hist(score_pos, bins=bins, color='red')
    plt.legend(['negatives', 'positives'])
    #plt.plot(x1[0])
    #plt.plot(x2[0])
    plt.show()

    print ('SGD', score)
    print(metrics.classification_report(y_test, y_pred, target_names=['pos', 'neg']))
    print(metrics.confusion_matrix(y_test, y_pred, labels=range(2)))


if __name__ == '__main__':
    evaluate()










