import cv2
import os
import cPickle
import sys
sys.path.append('/home/jacek/dev/keelab/DeepSport')
from db_metadata import get_rectangles
from annotations import rescale_annotation, draw_annotations
from deepsport_get_annot import get_gt_s3keys, \
                                get_meta_from_s3, \
                                get_image_from_s3, \
                                get_arena_list


def save_annotated_data_locally(arena, location='.'):
    im_dic = {}
    meta_dic = {}
    annot_dic = {}
    s3keys, num_cam = get_gt_s3keys(arena)

    if not os.path.isdir(location):
        raise(RuntimeError('Directory {} does not exist'.format(location)))
    if not os.path.isdir(location + '/images'):
        os.mkdir(location + '/images')

    for timestamp in s3keys.keys():
        im_dic[timestamp] = {}
        meta_dic[timestamp] = {}
        annot_dic[timestamp] = {}
        im, metadata, annot = None, None, None
        for viewnum, viewname in [(i, 'camcourt{}'.format(i + 1)) for i in range(0, num_cam)]:
            for el in s3keys[timestamp]:
                if type(el) == str and el.endswith('.json') and el.find(viewname) > 0:  # paths to s3 json
                    metadata = get_meta_from_s3(el)
                elif type(el) == str and el.endswith('_0.png') and el.find(viewname) > 0:  # paths to s3 images
                    im = get_image_from_s3(el)
                elif type(el) == list:  # annotations
                    annot = el[viewnum]

            if not im.size or not metadata or not annot:
                print('error missing metadata or annot or image for ts={}'.format(timestamp))
                continue

            rescale_annotation(annot, im.shape[1], im.shape[0])
            draw_annotations(im, annot)
            cv2.imshow('s3', im)
            cv2.imwrite(location + '/images/im_{}_{}.png'.format(viewnum, timestamp), im)

            meta_dic[timestamp][viewnum] = metadata
            annot_dic[timestamp][viewnum] = annot

    output = open(location + '/meta_dic.pkl', 'wb')
    cPickle.dump(meta_dic, output)
    output.close()
    output = open(location + '/annot_dic.pkl', 'wb')
    cPickle.dump(annot_dic, output)
    output.close()


def save_annotated_image_locally(location, folder_name, arenas, min_annotation_timestamp, single_image_per_arena=False):
    """
    Save images and GT for arenas
    @param : location : (str) root folder to save to
    @param : folder_name : (str) leaf folder to save to
    @param : arenas : (list) list of arena names
    @param : min_annotation_timestamp : (int) only annotation after this ts are saved
    @param : single_image_per_arena : (bool) save only one image per arena (the earliest ts)
    """
    im_dic = {}
    meta_dic = {}
    annot_dic = {}

    for arena in arenas:
        s3keys, num_cam = get_gt_s3keys(arena, min_annotation_ts=min_annotation_timestamp)

        if not os.path.isdir(location):
            raise(RuntimeError('Directory {} does not exist'.format(location)))
        if not os.path.isdir(location + '/' + folder_name):
            os.mkdir(location + '/' + folder_name)

        for timestamp in sorted(s3keys.keys()):
            im_dic[timestamp] = {}
            meta_dic[timestamp] = {}
            annot_dic[timestamp] = {}
            im, metadata, annot = None, None, None
            for viewnum, viewname in [(i, 'camcourt{}'.format(i + 1)) for i in range(0, num_cam)]:
                for el in s3keys[timestamp]:
                    if type(el) == str and el.endswith('.json') and el.find(viewname) > 0:  # paths to s3 json
                        metadata = get_meta_from_s3(el)
                    elif type(el) == str and el.endswith('_0.png') and el.find(viewname) > 0:  # paths to s3 images
                        im = get_image_from_s3(el)
                    elif type(el) == list:  # annotations
                        annot = el[viewnum]

                if not im.size or not metadata or not annot:
                    print('error missing metadata or annot or image for ts={}'.format(timestamp))
                    continue

                rescale_annotation(annot, im.shape[1], im.shape[0])
                #draw_annotations(im, annot)
                #cv2.imshow('s3', im)
                cv2.imwrite(location + '/' + folder_name + '/im_{}_{}.png'.format(viewnum, timestamp), im)

                meta_dic[timestamp][viewnum] = metadata
                annot_dic[timestamp][viewnum] = annot
            if single_image_per_arena:
                break

    output = open(location + '/' + folder_name + '/meta_dic.pkl', 'wb')
    cPickle.dump(meta_dic, output)
    output.close()
    output = open(location + '/' + folder_name + '/annot_dic.pkl', 'wb')
    cPickle.dump(annot_dic, output)
    output.close()


def save_annotated_test_images():
    """
    Save a few full images + annotations for testing a cnn
    Images are annotated after 20.12.2017 (and training vignettes are extracted from data before)
    """
    location = '.'
    folder_name = 'test_images'
    arenas = ['KS-FR-NANTES', 'KS-BE-MONS', 'KS-FI-SALO', 'KS-AT-VIENNA']
    min_annot_ts = 1513694341000 # data annotated after 20/12/2017
    save_annotated_image_locally(location, folder_name, arenas, min_annot_ts)


def save_hockey_test_images():
    """
    Save a few HOCKEY full images + annotations for testing a cnn
    """
    location = '.'
    folder_name = 'hockey_test_images'
    arenas = ['KS-US-HARBORCENTER1']
    min_annot_ts = 1513293341000 # with this value we get 11 items
    save_annotated_image_locally(location, folder_name, arenas, min_annot_ts)


def save_annotated_training_images():
    """ Save a few images for training (generate negatives)
    """
    arenas = get_arena_list()
    arenas2remove = ['KS-FR-NANTES', 'KS-BE-MONS', 'KS-FI-SALO', 'KS-AT-VIENNA']
    for a in arenas2remove:
        arenas.remove(a)
    location = '.'
    folder = 'train_images'
    min_annot_ts = 0
    save_annotated_image_locally(location, folder, arenas, min_annot_ts, True)


def show_saved_images(folder, location='.'):
    mypath = location + '/' + folder
    f2 = open(mypath + '/meta_dic.pkl', 'rb')
    f3 = open(mypath + '/annot_dic.pkl', 'rb')
    meta_dic = cPickle.load(f2)
    annot_dic = cPickle.load(f3)
    f2.close()
    f3.close()

    for ts in sorted(meta_dic.keys()):
        for view in meta_dic[ts]:
            annot = annot_dic[ts][view]
            metadata = meta_dic[ts][view]
            min_score = 0.14
            rects = get_rectangles(metadata, min_score=min_score, offset=0)
            im = cv2.imread(mypath + '/im_{}_{}.png'.format(view, ts))
            for rect in rects:
                cv2.rectangle(im, rect[0:2], rect[2:4], (0, 255, 0))
            draw_annotations(im, annot)
            cv2.imshow('cc', im)
            cv2.waitKey()


if __name__ == '__main__':
    save_hockey_test_images()
    show_saved_images('hockey_test_images')