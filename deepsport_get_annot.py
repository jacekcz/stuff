import boto3, botocore, requests
from boto3.dynamodb.conditions import Key
import cv2
import numpy as np
import json
import sys
sys.path.append('../../dev/producer/tools')

from deepsport_upload import make_s3_keys_from_deepsport_entry

s3rootkey = "DeepSport-dataset/"

# it is better to open unique a session for all queries
s_static = boto3.Session(profile_name='image-processing-developer')

def get_image_from_s3(s3key):
    s3, bucketname = get_s3resource()
    obj = s3.Object(bucketname, s3rootkey + s3key)
    img_str = obj.get()['Body'].read()
    nparr = np.fromstring(img_str, np.uint8)
    img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    return img_np


def get_meta_from_s3(s3key):
    s3, bucketname = get_s3resource()
    try:
        obj = s3.Object(bucketname, s3rootkey + s3key)
        j_str = obj.get()['Body'].read()
    except Exception:
        raise(RuntimeError("Key {} does not exist on the bucket {}".format (s3rootkey + s3key, bucketname)))
    return json.loads(j_str)


def init_table_gt():
    dyn = s_static.resource('dynamodb')
    return dyn.Table('DeepSport-dataset-GT')


def get_s3resource():
    # choose session
    session = boto3.Session(profile_name='production')
    # get s3 resource
    s3 = session.resource('s3')
    # default bucket
    s3_bucket = 'km-arena-data-euw1'
    bucketname = s3_bucket
    return s3, bucketname


def get_arena_list():
    arenas = []
    dyn = s_static.resource('dynamodb')
    table = dyn.Table('arena-list')
    response = table.scan()
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        items = response['Items']
        for item in items:
            arenas.append(item['arena_label'])
    else:
        raise requests.exceptions.HTTPError
    return arenas


def query(arena, s3rootkey=s3rootkey):
    s3, bucketname = get_s3resource()

    s3_keys = []
    table = init_table_gt()
    response = table.query(KeyConditionExpression=Key('arena_label').eq(arena))
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        items = response['Items']
        for item in items:
            if item.has_key('ground_truth'):
                print (item['ground_truth'])
                print (type(item['ground_truth']))
                for view in item['ground_truth']:
                    print (view)
                    for k,it_it in enumerate(view['annotations']):
                        print ('Annot {}'.format(k))
                        if it_it['type'] == 'player':
                            print (it_it['head'])
                            print (it_it['feet'])
                            print (it_it['hipsOrientation'])
                            print (it_it['headOrientation'])
                        elif it_it['type'] == 'ball':
                            print (it_it['center'])
                        else:
                            print ('Type is {}'.format(it_it['type']))

                s3_keys = make_s3_keys_from_deepsport_entry(item)

                print (s3_keys)
                for ke in s3_keys:
                    for viewnum, viewname in [(0,'camcourt1'), (1,'camcourt2')]:
                        if ke.endswith('_0.png') and ke.find(viewname) > 0:
                            obj = s3.Object(bucketname, s3rootkey + ke)
                            img_str = obj.get()['Body'].read()
                            nparr = np.fromstring(img_str, np.uint8)
                            img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

                            for k, it_it in enumerate(item['ground_truth'][viewnum]['annotations']):
                                if it_it['type'] == 'player':
                                    cv2.line(img_np,
                                             (int(it_it['head']['x']),int(it_it['head']['y'])),
                                             (int(it_it['feet']['x']), int(it_it['feet']['y'])),
                                             (255,0,0), 2)
                                else:
                                    cv2.circle(img_np, (int(it_it['center']['x']),int(it_it['center']['y'])),
                                               5,
                                               (0,255,0), 3)

                            cv2.imshow('s3', img_np)
                            cv2.waitKey()

    else:
        raise requests.exceptions.HTTPError
    return s3_keys


def get_gt_s3keys(arena, min_annotation_ts=0, max_annotation_ts=sys.maxint):
    s3_keys = {}
    num_cameras = None
    table = init_table_gt()
    response = table.query(KeyConditionExpression=Key('arena_label').eq(arena))
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        items = response['Items']
        for item in items:
            if 'annotation_ts' not in item:
                continue
            elif int(item['annotation_ts']) < min_annotation_ts or int(item['annotation_ts']) > max_annotation_ts:
                continue

            if 'ground_truth' in item:
                pair = make_s3_keys_from_deepsport_entry(item, as_dict=True)
                s3_keys.update([pair])
                s3_keys[pair[0]].append(item['ground_truth'])

            if item.has_key('num_cameras'):
                num_cameras = int(item['num_cameras'])

    else:
        raise requests.exceptions.HTTPError
    if not num_cameras:
        print ('Warning : could not find num camera for arena {}'.format(arena))
    return s3_keys, num_cameras


def test_get_gt_s3keys(id):
    s3keys = get_gt_s3keys('KS-BE-SPIROU')

    for ke in s3keys.keys():
        for el in s3keys[ke]:
            if type(el) == str:  # paths to s3 images
                if id == 0:
                    print (el)
            elif type(el) == list:  # annotations
                if id == 1:
                    print (el[0])


if __name__ == '__main__':

    # get all s3 keys for this arena from GT DB
    # s3keys = query('KS-BE-SPIROU', 'tmp/jcz/')

    # test get_gt_s3keys
    test_get_gt_s3keys(0)
    test_get_gt_s3keys(1)

