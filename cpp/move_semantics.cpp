
#include <iostream>
#include <vector>


class MyClass
{
public:
    MyClass() : a(0)
    {
        std::cout << "ctr called" << std::endl;
    }

    MyClass(int bb) : a(0), b(bb)
    {
        std::cout << "2d ctr called" << std::endl;
    }

    MyClass(const MyClass & mc)
    {
        a = mc.a;
        b = mc.b;
        std::cout << "copy " << std::endl;
    }

    //MyClass(MyClass&& v) = default;

    MyClass(MyClass&& v)
    {
        a = std::move(v.a); // it makes no sense to move POD. A better example would be if MyClass 
        b = std::move(v.b); // had a string or vector inside then moving makes sense...
        std::cout << "move " << std::endl;
    }
    int a;
    int b;
};

// example of returning a large object.
// in old c++ this required a copy
std::vector<MyClass> makeVec(size_t size)
{
    std::vector<MyClass> v(size);
    return v;
}


void mv_semantics_1()
{
    std::cout << "empty vec" << std::endl;
    std::vector<MyClass> v;
    v.reserve(10); // avoid copying the vector as it grows

    std::cout << "emplace_back (should use move because we emplace an rvalue) " << std::endl;
    v.emplace_back(MyClass());
    std::cout << "push_back (uses move) " << std::endl;
    v.push_back(MyClass());

    std::cout << "=====" << std::endl;
    MyClass mc; // this is an lvalue
    std::cout << "emplace_back or push_back cannot use move because mc is llvalue " << std::endl;
    v.push_back(mc);

    std::cout << "+++++" << std::endl;
    
    std::cout << "mc is llvalue but we move it" << std::endl;
    v.push_back(std::move(mc));

}



int main()
{
    std::cout << "==== starting mv_semantics_1 === " << std::endl;
    mv_semantics_1();
    std::cout << "==== end mv_semantics_1 === " << std::endl;

    
    std::cout << "ctr intialiser list" << std::endl;
    std::vector<MyClass> v2{ MyClass(), MyClass(), MyClass(1), MyClass(2)};


    //std::vector<MyClass> vv = makeVec(5);
    std::cout << "=== makeVec === " << std::endl;
    auto vv = makeVec(5);

    return 0;
}