import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')

import numpy
import cv2



def Test():
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector( cv2.HOGDescriptor_getDefaultPeopleDetector() )

    fn = 'im.png'
    imgt = cv2.imread(fn)

    img = cv2.resize(imgt,(64,128), 0., 0., cv2.INTER_AREA)
    cv2.imshow('img', img)
    ch = cv2.waitKey()
    if img is None:
        print('Failed to load image file:', fn)

    found, w = hog.detectMultiScale(img, winStride=(8,8), padding=(32,32), scale=1.05)

    print found
    print w

    # detect(img[, hitThreshold[, winStride[, padding[, searchLocations]]]]) -> foundLocations, weights
    found, w = hog.detect(img, hitThreshold=-100., winStride=(8,8), padding=(8,8))

    print found
    print w


def draw_detections(img, rects, thickness=1):
    for x, y, w, h in rects:
        # the HOG detector returns slightly larger rectangles than the real objects.
        # so we slightly shrink the rectangles to get a nicer output.
        pad_w, pad_h = int(0.15*w), int(0.05*h)
        cv2.rectangle(img, (x+pad_w, y+pad_h), (x+w-pad_w, y+h-pad_h), (0, 255, 0), thickness)



def HOGDetectC(imgt):
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    img = cv2.resize(imgt, (64, 128), 0., 0., cv2.INTER_AREA)
    #cv2.imshow('img', img)
    #ch = cv2.waitKey()

    # detect(img[, hitThreshold[, winStride[, padding[, searchLocations]]]]) -> foundLocations, weights
    found, w = hog.detect(img, hitThreshold=-1.0, winStride=(8, 8), padding=(8, 8))

    rects = [list(f) + [64,128] for f in found]

    draw_detections(img, rects)

    cv2.imshow('img', img)
    ch = cv2.waitKey()

def HOGDetect(imgt, hog):

    single_res = True
    if single_res:
        img = cv2.resize(imgt, (64, 128), 0., 0., cv2.INTER_AREA)
    else:
        img = imgt

    # cv2.imshow('img', img)
    # ch = cv2.waitKey()

    # detect(img[, hitThreshold[, winStride[, padding[, searchLocations]]]]) -> foundLocations, weights
    if single_res:
        found, w = hog.detect(img, hitThreshold=-1.0, winStride=(8, 8), padding=(8, 8))
        rects = [list(f) + [64, 128] for f in found]
    else:
        rects, w = hog.detectMultiScale(img, hitThreshold=-1.0, winStride=(8, 8), padding=(32, 32), scale=1.05)

    draw_detections(img, rects)

    cv2.imshow('img', img)
    ch = cv2.waitKey()



        #print found
    #print w


if __name__ == '__main__':
    fn = 'im.png'
    imgt = cv2.imread(fn)

    HOGDetect(imgt)






    #draw_detections(img, found)
#        draw_detections(img, found_filtered, 3)
#        print('%d (%d) found' % (len(found_filtered), len(found)))
#        cv2.imshow('img', img)
#        ch = cv2.waitKey()
#        if ch == 27:
#            break
#    cv2.destroyAllWindows()