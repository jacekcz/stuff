
from __future__ import print_function
import numpy as np
import sys
sys.path.append('/home/jacek/dev/keelab/DeepSport')
from db_metadata import get_calibration


def l1(x, y, w):
    d = 0
    for xx, yy, ww in zip(x, y, w):
        d += abs(xx - yy)
    return d


def l2sq_weighted(x, y, w):
    d = 0
    for xx, yy, ww in zip(x, y, w):
        d += ww * (xx - yy)**2
    return d


def l2_weighted(x, y, w):
    d = 0
    for xx, yy, ww in zip(x, y, w):
        d += ww * (xx - yy)**2
    return np.sqrt(d)


class LabelDist(object):
    def __init__(self, w1=.5, dist='l2'):
        if w1 > 1 or w1 < 0:
            raise RuntimeError('Wrong w1')
        self.w1 = w1
        self.w2 = 1 - w1
        if dist == 'l1':
            self.norm = l1
        elif dist == 'l2w':
            self.norm = l2_weighted

    def compute(self, det, gt, w=1):
        if w == 1:
            w = [1] * len(det)
        d = self.w1 * self.norm(det[0:2],gt[0:2],w) + self.w2 * self.norm(det[2:],gt[2:],w)
        return d


def find_3d_pt(P, R, t, pt2d, height_cm):
    """
    Another method than function find3d
    :param P: camera projection mat
    :param R: rotation mat
    :param t: translation part of P
    :param pt2d: input 2d
    :param height_cm: height
    :return: X,Y coord of point
    """
    pt2d_homo = np.array((pt2d[0], pt2d[1], 1.0))
    Ptmp = np.hstack((P[:, 0:2], P[:, 3:4]))
    p0 = np.linalg.solve(Ptmp, pt2d_homo)
    p0 = (p0 / p0[2])
    p0[2] = 0
    c0 = np.dot(-R.T,t)[:,0]
    lam = -height_cm / c0[2]
    print(p0.shape)
    xx = lam * (p0 - c0) + p0
    return xx


class SimpleDist(LabelDist):
        def __init__(self):
            print('simple_dist')

        def compute(self, x,y):
            print('compute s')
            d = 0
            for xx, yy in zip(x, y):
                d += abs(xx - yy)

class SSDist(LabelDist):
    def __init__(self):
        print('ss_dist')

    def compute(self, x,y):
        print('compute ss')
        d = 0
        for xx, yy in zip(x, y):
            d += (xx - yy)**1.5


if __name__ == '__main__':

    P = np.array([[  4.16716485e+02,  -1.62941489e+03,   8.55455054e+02,   4.01570511e+06],
                  [ -3.95994187e+02,   2.64448155e+01,   1.71844734e+03,   2.24173433e+06],
                  [ -5.88707000e-01,  -6.55743000e-01,   4.72678000e-01,   3.99513696e+03]])

    pt2d_1 = (1805,1148)

    pt2d_2 = (1796, 1124)

    import cPickle
    f2 = open('meta_dic.pkl', 'rb')
    meta_dic = cPickle.load(f2)
    f2.close()

    ts = sorted(meta_dic.keys())[0]
    metadata = meta_dic[ts][0]
    P2, K, R, t = get_calibration(metadata)

    pt2d_3 = (1634, 869)
    pt2d_4 = (1633, 782)

    h = -145

    X1 = find_3d_pt(P2, R, t, pt2d_3, 0)
    X2 = find_3d_pt(P2, R, t, pt2d_4, h)

    print(X1)
    print(X2)

    dp = DistOnGroundPlane(P2)
    X3 = dp.find3d(pt2d_4, h)
    print(X3)

    x = np.dot(P2,np.array( (X2[0], X2[1], h, 1)))
    print(x[0]/x[2], x[1]/x[2])

    if 0:
        d = dp.compute((0,0,1805,1148), (0,0,1796,1124), (1,1))
        print(d)


    exit(0)
    det = [0,0, 2, 3]
    gt = [0,0.5, 2, 3]
    ld = LabelDist(w1=0.1,dist='l1')

    print(ld.compute(det,gt))
